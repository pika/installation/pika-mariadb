ALTER TABLE footprint_base
ADD COLUMN IF NOT EXISTS energy_in_kwh DOUBLE DEFAULT NULL,
ADD COLUMN IF NOT EXISTS energy_efficiency_in_gflops_per_watt DOUBLE DEFAULT NULL;

ALTER TABLE footprint_analysis
ADD COLUMN IF NOT EXISTS unused_mem FLOAT DEFAULT 0;
