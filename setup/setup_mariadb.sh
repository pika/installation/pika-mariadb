#!/bin/bash

admin_password_request ()
{
  echo "Please enter a password for the user 'admin' and confirm it with [ENTER]."
  unset PWD;
  while IFS= read -r -s -n1 pass; do
    [[ -z $pass ]] && { printf '\n'; break; } 
    if [[ $pass == $'\x7f' ]]; then
      [[ -n $PWD ]] && PWD=${PWD%?}
      printf '\b \b'
    else
      printf '*'
      PWD+=$pass
    fi
  done
  ADMINPW=$PWD
}

read_password_request ()
{
  echo "Please enter a password for the user 'readonly' and confirm it with [ENTER]."
  unset PWD;
  while IFS= read -r -s -n1 pass; do
    [[ -z $pass ]] && { printf '\n'; break; } 
    if [[ $pass == $'\x7f' ]]; then
      [[ -n $PWD ]] && PWD=${PWD%?}
      printf '\b \b'
    else
      printf '*'
      PWD+=$pass
    fi
  done
  READPW=$PWD
}

error_check()
{
  if [ $1 -ne 0 ]; then
    echo "Creating the pika database failed."
    exit 1
  fi
}

admin_password_request
read_password_request

commands="
CREATE DATABASE pika;
CREATE USER 'admin' IDENTIFIED BY '${ADMINPW}';
GRANT USAGE ON *.* TO 'admin'@'localhost' IDENTIFIED BY '${ADMINPW}';
GRANT ALL privileges ON pika.* TO 'admin'@'localhost';
GRANT USAGE ON *.* TO 'admin'@'%' IDENTIFIED BY '${ADMINPW}';
GRANT ALL privileges ON pika.* TO 'admin'@'%';
CREATE USER 'readonly' IDENTIFIED BY '${READPW}';
GRANT USAGE ON *.* TO 'readonly'@'localhost' IDENTIFIED BY '${READPW}';
GRANT SELECT ON pika.* TO 'readonly'@'localhost';
GRANT USAGE ON *.* TO 'readonly'@'%' IDENTIFIED BY '${READPW}';
GRANT SELECT ON pika.* TO 'readonly'@'%';
FLUSH PRIVILEGES;
"

echo "${commands}" | /usr/bin/mariadb -p${MYSQL_ROOT_PASSWORD}
error_check $?

sleep 3

mariadb -D pika -p${MYSQL_ROOT_PASSWORD} < /pika-setup/pika_structure.sql
error_check $?

echo "Pika database successfully created."
exit 0

