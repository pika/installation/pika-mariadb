### 1. Install MariaDB and Adminer

```sh
git clone https://gitlab.hrz.tu-chemnitz.de/pika/packages/pika-mariadb.git
```

The following script installs the container for MariaDB and Adminer. It also sets up the pika database.

First change the root password for MariaDB.

```sh
vi docker-compose.yml
MYSQL_ROOT_PASSWORD=pika123
```

```sh
./install.sh
```

Add MariaDB access port to your firewall policy, e.g. with
```
firewall-cmd --zone=public --permanent --add-port=3306/tcp
systemctl restart firewalld
```

### 2. Open Adminer in the browser

Adminer is a web-based tool for managing content in MariaDB.
You can use this tool to get access to the database.

```sh
http://<host ip address>:8081
System: MySQL
Server: pika-mariadb
Username: admin
Database: pika
```

You can also access the database with mysql CLI:
```sh
mysql -h <host ip address> -P 3306 -u admin -p<password>
```
